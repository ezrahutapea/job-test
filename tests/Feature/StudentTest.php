<?php

namespace Tests\Feature;

use App\SchoolClass;
use App\Student;
use App\Teacher;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StudentTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function access_page_to_view_list_students()
    {
        // user must logged in
        $this->actingAs(factory(User::class)->make());

        // when hit the endpoint /students , then accessing page to view list of students
        $this->get('students')->assertViewIs('students.index');
    }

    /** @test */
    public function access_page_to_create_a_student()
    {
        // user must logged in
        $this->actingAs(factory(User::class)->make());

        // when hit the endpoint /students/create , then accessing page to create a new student
        $this->get('students/create')->assertViewIs('students.create');
    }

    /** @test */
    public function validation_fails_when_create_a_student()
    {
        // user must logged in
        $this->actingAs(factory(User::class)->make());

        // when hit the endpoint /students to create a new student
        $response = $this->post('students');

        // Then there is should be an errors;

        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function create_a_student()
    {
        // user must logged in
        $this->actingAs(factory(User::class)->make());

        $attributes = ['name'  => 'Student 1'];

        // when hit the endpoint /students to create a new student
        $this->post('students', $attributes);

        // Then there is should be a new student in database;

        $this->assertDatabaseHas('students', $attributes);
    }

    /** @test */
    public function validation_fails_when_update_a_student()
    {
        // user must logged in
        $this->actingAs(factory(User::class)->make());

        factory(Student::class)->create();

        $student    = Student::latest()->first();

        // when hit the endpoint /students/{id} to update a student
        $response = $this->put(url('students', $student->id));

        // Then there is should be an errors;

        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function update_a_student()
    {
        // user must logged in
        $this->actingAs(factory(User::class)->make());

        factory(Student::class)->create();

        $student    = Student::latest()->first();
        $attributes = ['name'  => 'Student 1 Updated'];

        // when hit the endpoint /students/{id} to update a student
        $this->put(url('students', $student->id), $attributes);

        // Then the student should be updated in database;

        $this->assertDatabaseHas('students', $attributes);
    }

    /** @test */
    public function delete_a_student_that_have_class_related_and_only_this_one_student_in_class()
    {
        $this->withoutExceptionHandling();
        // user must logged in
        $this->actingAs(factory(User::class)->make());

        // create dummy student , teacher and a school class
        factory(Student::class)->create();
        factory(Teacher::class)->create()->each(function ($student) {
            $student->schoolClass()->save(factory(SchoolClass::class)->make());
        });

        $class      = SchoolClass::latest()->first();

        $student    = Student::latest()->first();

        $class->students()->attach($student->id);

        // when hit the endpoint /students/{id} to delete a student
        $response   = $this->delete(url('students', $student->id));

        // Then there is should be an errors;

        $response->assertSessionHas('error-message', 'Cannot delete <strong>'.$student->name.'</strong>, because class that related to this student just have this one student.<br>Class must have at least one student.');
    }

    /** @test */
    public function delete_a_student_that_not_have_class_related_or_have_class_related_but_have_many_students_in_class()
    {
        // user must logged in
        $this->actingAs(factory(User::class)->make());

        factory(Student::class)->create();

        $student    = Student::latest()->first();

        $id         = $student->id;

        // when hit the endpoint /students/{id} to delete a student
        $response = $this->delete(url('students', $student->id));

        // Then the student should be a removed from database;

        $this->assertDatabaseMissing('students', compact('id'));

        $response->assertSessionHas('success-message', '<strong>'.$student->name.'</strong> has deleted.');
    }
}
