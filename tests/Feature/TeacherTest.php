<?php

namespace Tests\Feature;

use App\SchoolClass;
use App\Teacher;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TeacherTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function access_page_to_view_list_teachers()
    {
        // user must logged in
        $this->actingAs(factory('App\User')->make());

        // when hit the endpoint /teachers , then accessing page to view list of teachers
        $this->get('teachers')->assertViewIs('teachers.index');
    }

    /** @test */
    public function access_page_to_create_a_teacher()
    {
        // user must logged in
        $this->actingAs(factory('App\User')->make());

        // when hit the endpoint /teachers/create , then accessing page to create a new teacher
        $this->get('teachers/create')->assertViewIs('teachers.create');
    }

    /** @test */
    public function validation_fails_when_create_a_teacher()
    {
        // user must logged in
        $this->actingAs(factory('App\User')->make());

        // when hit the endpoint /teachers to create a new teacher
        $response = $this->post('teachers');

        // Then there is should be an errors;

        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function create_a_teacher()
    {
        // user must logged in
        $this->actingAs(factory('App\User')->make());

        $attributes = ['name'  => 'Teacher 1'];

        // when hit the endpoint /teachers to create a new teacher
        $this->post('teachers', $attributes);

        // Then there is should be a new teacher in database;

        $this->assertDatabaseHas('teachers', $attributes);
    }

    /** @test */
    public function validation_fails_when_update_a_teacher()
    {
        // user must logged in
        $this->actingAs(factory('App\User')->make());

        factory(Teacher::class)->create();

        $teacher    = Teacher::latest()->first();

        // when hit the endpoint /teachers/{id} to update a teacher
        $response = $this->put(url('teachers', $teacher->id));

        // Then there is should be an errors;

        $response->assertSessionHasErrors(['name']);
    }

    /** @test */
    public function update_a_teacher()
    {
        // user must logged in
        $this->actingAs(factory('App\User')->make());

        factory(Teacher::class)->create();

        $teacher    = Teacher::latest()->first();
        $attributes = ['name'  => 'Teacher 1 Updated'];

        // when hit the endpoint /teachers/{id} to update a teacher
        $this->put(url('teachers', $teacher->id), $attributes);

        // Then teacher should be updated in database;

        $this->assertDatabaseHas('teachers', $attributes);
    }

    /** @test */
    public function delete_a_teacher_that_have_class_related()
    {
        // user must logged in
        $this->actingAs(factory('App\User')->make());

        factory(Teacher::class)->create()->each(function ($teacher) {
            $teacher->schoolClass()->save(factory(SchoolClass::class)->make());
        });

        $teacher    = Teacher::latest()->first();

        // when hit the endpoint /teachers/{id} to delete a teacher
        $response   = $this->delete(url('teachers', $teacher->id));

        // Then there is should be an errors;

        $response->assertSessionHas('error-message', 'Cannot delete <strong>'.$teacher->name.'</strong>, because this teacher has class related.');
    }

    /** @test */
    public function delete_a_teacher_that_not_have_class_related()
    {
        // user must logged in
        $this->actingAs(factory('App\User')->make());

        factory(Teacher::class)->create();

        $teacher    = Teacher::latest()->first();

        $id         = $teacher->id;

        // when hit the endpoint /teachers/{id} to delete a teacher
        $response = $this->delete(url('teachers', $teacher->id));

        // Then teacher should be removed from database;

        $this->assertDatabaseMissing('teachers', compact('id'));

        $response->assertSessionHas('success-message', '<strong>'.$teacher->name.'</strong> has deleted.');
    }
}
