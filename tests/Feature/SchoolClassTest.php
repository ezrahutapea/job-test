<?php

namespace Tests\Feature;

use App\SchoolClass;
use App\Student;
use App\Teacher;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SchoolClassTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function access_page_to_view_list_classes()
    {
        // user must logged in
        $this->actingAs(factory('App\User')->make());

        // when hit the endpoint /classes , then accessing page to view list of classes
        $this->get('classes')->assertViewIs('classes.index');
    }

    /** @test */
    public function access_page_to_create_a_class()
    {
        // user must logged in
        $this->actingAs(factory('App\User')->make());

        // when hit the endpoint /classes/create , then accessing page to create a new class
        $this->get('classes/create')->assertViewIs('classes.create');
    }

    /** @test */
    public function validation_fails_when_create_a_class()
    {
        // user must logged in
        $this->actingAs(factory('App\User')->make());

        // when hit the endpoint /classes to create a new class
        $response = $this->post('classes');

        // Then there is should be an errors;

        $response->assertSessionHasErrors(['teacher', 'name', 'students']);
    }

    /** @test */
    public function create_a_class()
    {
        // user must logged in
        $this->actingAs(factory('App\User')->make());

        $teacher = factory(Teacher::class)->create();
        $student = factory(Student::class)->create();

        $attributes = [
            'teacher'       => $teacher->id,
            'name'          => 'Class 1',
            'students'      => [ $student->id ]
        ];

        // when hit the endpoint /classes to create a new class
        $this->post('classes', $attributes);

        // Then there is should be a new class in database;

        $this->assertDatabaseHas('school_classes', [
            'teacher_id'    => $teacher->id,
            'name'          => 'Class 1',
        ]);

        $class = SchoolClass::latest()->first();

        // Then there is should be a new class relation to student in database;

        $this->assertDatabaseHas('class_students', [
            'class_id'    => $class->id,
            'student_id'  => $student->id,
        ]);
    }

    /** @test */
    public function validation_fails_when_update_a_class()
    {
        // user must logged in
        $this->actingAs(factory('App\User')->make());

        $teacher = factory(Teacher::class)->create();
        $student = factory(Student::class)->create();

        $class   = factory(SchoolClass::class)->create([
            'teacher_id'    => $teacher->id,
        ]);

        $class->students()->attach($student->id);

        // when hit the endpoint /classes/{id} to update a class
        $response = $this->put(url('classes', $class->id));

        // Then there is should be an errors;

        $response->assertSessionHasErrors(['teacher', 'name', 'students']);
    }

    /** @test */
    public function update_a_class()
    {
        // user must logged in
        $this->actingAs(factory('App\User')->make());

        $teacher    = factory(Teacher::class)->create();
        $student    = factory(Student::class)->create();

        $class      = factory(SchoolClass::class)->create([
            'teacher_id'    => $teacher->id,
        ]);

        $class->students()->attach($student->id);

        $attributes = [
            'teacher'       => $teacher->id,
            'name'          => 'Class 1 Updated',
            'students'      => [ $student->id ]
        ];

        // when hit the endpoint /classes/{id} to update a class
        $this->put(url('classes', $class->id), $attributes);

        // Then class should be updated in database;

        $this->assertDatabaseHas('school_classes', [
            'teacher_id'    => $teacher->id,
            'name'          => 'Class 1 Updated',
        ]);

        // Then class student relation should be updated in database;

        $this->assertDatabaseHas('class_students', [
            'class_id'    => $class->id,
            'student_id'  => $student->id,
        ]);
    }

    /** @test */
    public function delete_a_class()
    {
        // user must logged in
        $this->actingAs(factory('App\User')->make());

        $teacher    = factory(Teacher::class)->create();
        $student    = factory(Student::class)->create();

        $class      = factory(SchoolClass::class)->create([
            'teacher_id'    => $teacher->id,
        ]);

        $class->students()->attach($student->id);

        $id         = $class->id;

        // when hit the endpoint /classes/{id} to delete a class
        $response   = $this->delete(url('classes', $class->id));

        // Then the class relation to student should be a detached from database;
        $this->assertDatabaseMissing('class_students', [
            'class_id'      => $id,
            'student_id'    => $student->id
        ]);

        // Then the class should be a removed from database;
        $this->assertDatabaseMissing('school_classes', compact('id'));

        $response->assertSessionHas('success-message', '<strong>'.$class->name.'</strong> has deleted.');
    }

}
