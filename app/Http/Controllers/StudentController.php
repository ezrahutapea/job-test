<?php

namespace App\Http\Controllers;

use App\SchoolClass;
use App\Student;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::paginate(10);
        return view('students.index', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('students.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        Student::create(request()->validate([ 'name'  => 'required' ]));

        return redirect('students');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        return view('students.edit', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Student $student)
    {
        $student->update(request()->validate([ 'name'  => 'required' ]));

        return redirect('students');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $myClass = $student->schoolClass()->first();

        if($myClass) {

            if($myClass->students()->where('id', '<>', $student->id)->count() === 0) {
                session()->flash('error-message', 'Cannot delete <strong>'.$student->name.'</strong>, because class that related to this student just have this one student.<br>Class must have at least one student.');
                return redirect()->back();
            }

        }

        $student->schoolClass()->detach();
        $student->delete();
        session()->flash('success-message', '<strong>'.$student->name.'</strong> has deleted.');

        return redirect('students');
    }
}
