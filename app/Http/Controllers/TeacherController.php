<?php

namespace App\Http\Controllers;

use App\Teacher;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers = Teacher::paginate(10);
        return view('teachers.index', compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('teachers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        Teacher::create(request()->validate([ 'name'  => 'required' ]));

        return redirect('teachers');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function edit(Teacher $teacher)
    {
        return view('teachers.edit', compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function update(Teacher $teacher)
    {
        $teacher->update(request()->validate([ 'name'  => 'required' ]));

        return redirect('teachers');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teacher $teacher)
    {
        if($teacher->schoolClass()->count() > 0) {
            session()->flash('error-message', 'Cannot delete <strong>'.$teacher->name.'</strong>, because this teacher has class related.');
            return redirect()->back();
        }

        $teacher->delete();
        session()->flash('success-message', '<strong>'.$teacher->name.'</strong> has deleted.');
        return redirect('teachers');
    }
}
