<?php

namespace App\Http\Controllers;

use App\SchoolClass;
use App\Student;
use App\Teacher;

class SchoolClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $classes = SchoolClass::paginate(10);
        return view('classes.index', compact('classes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $teachers = Teacher::all();
        $students = Student::whereDoesntHave('schoolClass')->get();
        return view('classes.create', compact('teachers', 'students'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        request()->validate([
            'teacher'   => 'required',
            'students'  => 'required|min:1',
            'name'      => 'required'
        ]);

        $teacher = Teacher::whereId(request('teacher'))->firstOrFail();

        $class = $teacher->schoolClass()->create([
            'name'  => request('name')
        ]);

        $class->students()->attach(request('students'));

        return redirect('classes');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SchoolClass  $schoolClass
     * @return \Illuminate\Http\Response
     */
    public function edit(SchoolClass $class)
    {
        $teachers   = Teacher::all();
        $myStudents = Student::whereIn('id', $class->students()->pluck('id')->toArray());
        $students   = Student::whereDoesntHave('schoolClass')
                            ->union($myStudents)
                            ->get();

        return view('classes.edit', compact('class', 'teachers', 'students'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SchoolClass  $schoolClass
     * @return \Illuminate\Http\Response
     */
    public function update(SchoolClass $class)
    {
        request()->validate([
            'teacher'   => 'required',
            'students'  => 'required|min:1',
            'name'      => 'required'
        ]);

        $teacher = Teacher::whereId(request('teacher'))->firstOrFail();

        $class->update([
            'teacher_id'  => $teacher->id,
            'name'      => request('name')
        ]);

        $class->students()->detach();
        $class->students()->attach(request('students'));

        return redirect('classes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SchoolClass  $schoolClass
     * @return \Illuminate\Http\Response
     */
    public function destroy(SchoolClass $class)
    {
        $class->students()->detach();
        $class->delete();
        session()->flash('success-message', '<strong>'.$class->name.'</strong> has deleted.');
        return redirect('classes');
    }

    public function downloadPdf()
    {
        $classes = SchoolClass::all();

        if($classes->count() === 0) {
            session()->flash('error-message', 'Nothing to download, class are empty.');
            return redirect()->back();
        }

        $pdf = \PDF::loadView('pdf-viewer', compact('classes'));
        return $pdf->download('class-list.pdf');
    }
}
