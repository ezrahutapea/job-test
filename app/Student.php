<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [ 'name' ];

    public function schoolClass()
    {
        return $this->belongsToMany('App\SchoolClass', 'class_students', 'student_id', 'class_id');
    }
}
