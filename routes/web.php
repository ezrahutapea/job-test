<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::middleware('auth')->group(function(){

    Route::resource('teachers', 'TeacherController');
    Route::resource('classes', 'SchoolClassController');
    Route::resource('students', 'StudentController');
    Route::get('/download-pdf', 'SchoolClassController@downloadPdf');

});

Route::get('register', function() { abort(404); });


