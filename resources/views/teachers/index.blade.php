@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Teacher Management
                        <a href="{{ url('teachers/create') }}" class="btn btn-success btn-sm float-right">Add new</a>
                    </div>

                    <div class="card-body">
                        <table id="teachers" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @include('layouts.session-messages')

                                @forelse($teachers as $teacher)
                                    <tr>
                                        <td style="width: 80%">{{ $teacher->name }}</td>
                                        <td style="width: 20%">

                                            <a href="{{ url('teachers', $teacher->id) }}/edit" class="btn btn-primary btn-sm text-white">Edit</a>

                                            <form style="display: inline-block;" id="form-delete-{{ $teacher->id }}" action="{{ url('teachers', $teacher->id) }}" method="POST">
                                                @csrf @method('DELETE')
                                            </form>

                                            <a
                                                class="btn btn-danger btn-sm text-white"
                                                onclick="event.preventDefault(); document.getElementById('form-delete-{{ $teacher->id }}').submit();"
                                                style="cursor: pointer;"
                                            >Delete</a>

                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="2" class="text-center">
                                            <em>No Data</em>
                                        </td>
                                    </tr>
                                @endforelse

                            </tbody>
                        </table>

                        <div class="float-right">
                            {{ $teachers->links() }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
