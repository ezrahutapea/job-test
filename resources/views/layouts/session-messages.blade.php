@if(session()->has('error-message'))

    <div class="alert alert-danger">
        {!! session()->get('error-message') !!}
    </div>

@endif

@if(session()->has('success-message'))

    <div class="alert alert-success">
        {!! session()->get('success-message') !!}
    </div>

@endif
