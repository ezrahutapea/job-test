<ul class="navbar-nav mr-auto">
    <li class="nav-item {{ request()->segment(1) === 'teachers' ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('teachers') }}">Teacher <span class="sr-only">(current)</span></a>
    </li>
    <li class="nav-item {{ request()->segment(1) === 'students' ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('students') }}">Student</a>
    </li>
    <li class="nav-item {{ request()->segment(1) === 'classes' ? 'active' : '' }}">
        <a class="nav-link" href="{{ url('classes') }}">Class</a>
    </li>
</ul>
