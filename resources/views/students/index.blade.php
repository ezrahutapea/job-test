@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Student Management
                        <a href="{{ url('students/create') }}" class="btn btn-success btn-sm float-right">Add new</a>
                    </div>

                    <div class="card-body">

                        @include('layouts.session-messages')

                        <table id="students" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @forelse($students as $student)
                                    <tr>
                                        <td style="width: 80%">{{ $student->name }}</td>
                                        <td style="width: 20%">

                                            <a href="{{ url('students', $student->id) }}/edit" class="btn btn-primary btn-sm text-white">Edit</a>

                                            <form style="display: inline-block;" id="form-delete-{{ $student->id }}" action="{{ url('students', $student->id) }}" method="POST">
                                                @csrf @method('DELETE')
                                            </form>

                                            <a
                                                class="btn btn-danger btn-sm text-white"
                                                onclick="event.preventDefault(); document.getElementById('form-delete-{{ $student->id }}').submit();"
                                                style="cursor: pointer;"
                                            >Delete</a>

                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="3" class="text-center">
                                            <em>No Data</em>
                                        </td>
                                    </tr>
                                @endforelse

                            </tbody>
                        </table>

                        <div class="float-right">
                            {{ $students->links() }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
