@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Student Management</div>

                    <div class="card-body">

                        @include('layouts.validation-errors')

                        <form action="{{ url('students') }}" method="POST">
                            @csrf
                            <div class="form-label-group">
                                <input class="form-control {{ $errors->has('name')? 'is-invalid' : ''}}" id="student-name" placeholder="Name" type="text" name="name" value="{{ old('name') }}">
                                <label for="student-name">Name</label>
                            </div>
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
