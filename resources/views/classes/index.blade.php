@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Class Management
                        <a href="{{ url('classes/create') }}" class="btn btn-success btn-sm float-right">Add new</a>
                        <a href="{{ url('download-pdf') }}">
                            <button class="btn btn-warning btn-sm float-right mr-2" {{ $classes->count() === 0? 'disabled' : '' }}>Download Pdf</button>
                        </a>
                    </div>

                    <div class="card-body">

                        @include('layouts.session-messages')

                        <table id="classes" class="table table-striped table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Teacher</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                @forelse($classes as $class)
                                    <tr>
                                        <td style="width: 40%">{{ $class->name }}</td>
                                        <td style="width: 40%">{{ $class->teacher->name }}</td>
                                        <td style="width: 20%">

                                            <a href="{{ url('classes', $class->id) }}/edit" class="btn btn-primary btn-sm text-white">Edit</a>

                                            <form style="display: inline-block;" id="form-delete-{{ $class->id }}" action="{{ url('classes', $class->id) }}" method="POST">
                                                @csrf @method('DELETE')
                                            </form>

                                            <a
                                                class="btn btn-danger btn-sm text-white"
                                                onclick="event.preventDefault(); document.getElementById('form-delete-{{ $class->id }}').submit();"
                                                style="cursor: pointer;"
                                            >Delete</a>

                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="3" class="text-center">
                                            <em>No Data</em>
                                        </td>
                                    </tr>
                                @endforelse

                            </tbody>
                        </table>

                        <div class="float-right">
                            {{ $classes->links() }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
