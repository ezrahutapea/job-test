@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Class Management</div>

                    <div class="card-body">

                        @include('layouts.validation-errors')

                        <form action="{{ url('classes', $class->id) }}" method="POST">
                            @csrf @method('PUT')

                            <div class="form-label-group">
                                <input class="form-control {{ $errors->has('name')? 'is-invalid' : ''}}" id="class-name" placeholder="Name" type="text" name="name" value="{{ old('name')?: $class->name }}">
                                <label for="class-name">Name</label>
                            </div>

                            <div class="form-label-group">
                                <select class="custom-select {{ $errors->has('teacher')? 'is-invalid' : ''}}" id="teacher-select" name="teacher">
                                    <option value="">Select Teacher</option>
                                    @foreach($teachers as $teacher)
                                        <option
                                            value="{{ $teacher->id }}"
                                            {{
                                                old('teacher') == $teacher->id? 'selected' :
                                                ($teacher->id == $class->teacher_id ? 'selected' : '')
                                            }}
                                        >{{ $teacher->name }}</option>
                                    @endforeach
                                </select>
                                <label for="teacher-select">Teacher</label>
                            </div>

                            <div class="form-label-group">
                                <select class="custom-select {{ $errors->has('students')? 'is-invalid' : ''}}" id="students-select" name="students[]" multiple>
                                    @if(old('students') || $errors->has('students'))
                                        @foreach($students as $student)
                                            <option
                                                value="{{ $student->id }}"
                                                {{ in_array($student->id, old('students')?: [])? 'selected' : '' }}
                                            >{{ $student->name }}</option>
                                        @endforeach
                                    @else
                                        @foreach($students as $student)
                                            <option
                                                value="{{ $student->id }}"
                                                {{ in_array($student->id, $class->students()->pluck('id')->toArray()) ? 'selected' : '' }}
                                            >{{ $student->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <label for="teacher-select">Students</label>
                            </div>

                            <button class="btn btn-primary" type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
