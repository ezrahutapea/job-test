<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <style type="text/css">

        body {
            margin: auto;
            width: 100%;
        }

        table {
            width: 100%;
        }

        .tg {
            border-collapse: collapse;
            border-spacing: 0;
            border-color: #aabcfe;
        }
        .tg td {
            font-family: Arial, sans-serif;
            font-size: 14px;
            padding: 10px 5px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            text-align: center;
        }
        .tg th {
            font-family: Arial, sans-serif;
            font-size: 14px;
            font-weight: normal;
            padding: 10px 5px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            border-color: #aabcfe;
            color: #039;
            background-color: #b9c9fe;
        }
        .tg .tg-u43i {
            background-color: #343434;
            color: #ffffff;
            border-color: #000000;
            text-align: center;
            vertical-align: top
        }
        .tg tr:nth-child(2) {
            font-family: Arial, sans-serif;
            font-size: 14px;
            padding: 10px 5px;
            border-style: solid;
            border-width: 1px;
            overflow: hidden;
            word-break: normal;
            background-color: #c7c4c4;
        }

    </style>
</head>
<body>
    <table class="tg">
        <tr>
            <th class="tg-u43i" colspan="4">Results</th>
        </tr>
        <tr>
            <td>No</td>
            <td>Class</td>
            <td>Teacher</td>
            <td>Students</td>
        </tr>
        @for($i = 0; $i < $classes->count(); $i++)
            <tr>
                <td>{{ $i + 1 }}</td>
                <td>{{ $classes[$i]->name }}</td>
                <td>{{ $classes[$i]->teacher->name }}</td>
                <td style="text-align: left;">
                    <ul>
                        @foreach($classes[$i]->students as $student)
                            <li>{{ $student->name }}</li>
                        @endforeach
                    </ul>
                </td>
            </tr>
        @endfor
    </table>
</body>
</html>
