<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchoolClassesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('school_classes', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('teacher_id');
            $table->string('name');
            $table->timestamps();

            $table->foreign('teacher_id')->references('id')->on('teachers');
        });

        Schema::create('class_students', function (Blueprint $table) {
            $table->unsignedInteger('class_id');
            $table->unsignedInteger('student_id');

            $table->foreign('class_id')->references('id')->on('school_classes');
            $table->foreign('student_id')->references('id')->on('students');

            $table->primary([ 'student_id' ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('class_students');
        Schema::dropIfExists('school_classes');
    }
}
