<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateTable();
        factory(App\User::class, 1)->create();
    }

    protected function truncateTable()
    {
        DB::table('users')->truncate();
    }
}
